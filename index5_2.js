class person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}     
p1 = new person("Max", 33);
p2 = new person("Anna", 23);
p3 = new person("Angela", 19);
p4 = new person("Vitya", 77);
p5 = new person("Luna", 10); 

var persons = [p1, p2, p3, p4, p5];
        console.log("!!!Increasing of years!!!");
persons.sort((p1, p2) => p1.age - p2.age);
persons.forEach(e => {
        console.log(e.name+ " " + e.age);
});
        console.log(" ");       
        console.log("!!!Descending of years!!!");
persons.sort((p1, p2) => p2.age - p1.age);
persons.forEach(e => {
    console.log(e.name+ " " + e.age);
});
