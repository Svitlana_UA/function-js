class person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
function compareAge(person1, person2) {
    if (!(person1 instanceof person))
        throw "person1 is not instance of person!";
    if (!(person2 instanceof person))
        throw "person2 is not instance of person!";
    var text;
    if (person1.age > person2.age)
        text = "older";
    else if (person1.age < person2.age)
        text = "younger";
    else
        text = "the same age as";

    return `${person1.name} is ${text} ${person2.name}`;
}
// <Person 1 name> is <older | younger | the same age as> <Person 2 name>
var person1 = new person("Anna", 23);
var person2 = new person("Luna", 10);

console.log(compareAge(person1, person2) );

//var person1 = new person("Vasya", 33);
//var person2 = new person("Max", 33);
 
//var person1 = new person("Anna", 23);
//var person2 = new person("Max", 33);

//var person1 = new person("Anna", 23);
//var person2 = new person("Luna", 10);