function countTrue(boolArray) {
    var count = 0;
    boolArray.forEach(element => {
        if (element == true)
            count++;
    });
    return count;
}

var boolArray = ([true, false, false, true, false])
console.log(countTrue(boolArray) );

//  ([true, false, false, true, false, false, false, true])           
//  ([true, false, false, true, false])
//  ([false, false, false, false])
//  ([]) 