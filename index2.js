// Сумма двух массивов
function sumArray(a, b) {
    var result = 0;
    a.forEach(el => {
        result = result + el;
    });
    b.forEach(el => {
        result = result + el;
    });
    return result;
}

var a = [1, 2, 3, -5, 0, 10];
var b = [5, -1, 7];

console.log(sumArray(a, b) );

// var a = [1, 2, 3, -5, 0, 10];
// var b = [5, -1, 7];

// var a = [1, 2, 3, -5, 0, 10, 5, 10, 15, 20, -15, -10, 3];
// var b = [5, -1, 7, 3, 5, -10, 20, -20, 2];