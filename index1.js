function myFunction(a, b){
    if (isNaN(a))
       throw "A is not a number";
    if (isNaN(b))
       throw "B is not a number";
    if (a < b) { 
        return a + b;}  
    
    else if (a > b) { 
        return a - b;}   
    
    else {
        return a* b;}  
    }

    try {
        let result = myFunction(10, 20);  
        console.log(result);
    } catch (error) {
        console.log(error);
    }
    
    //   myFunction(10, 20); 
    //   myFunction(20, 10); 
    //   myFunction(10, 10); 
    //   myFunction(-10, 10);
    //   myFunction(10, -10);
    //   myFunction("A", 10);