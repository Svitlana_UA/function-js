// Сумма элементов двух массивов
function sumArray (a, b) {
    var c = [];
    for (var i = 0; i < Math.max(a.length, b.length); i++) {
      c.push((a[i] || 0) + (b[i] || 0));
    }
    return c; 
  }

var a = [1, 2, 3, -5, 0, 10];
var b = [5, -1, 7];

console.log(sumArray(a, b) );


// var a = [1, 2, 3, -5, 0, 10];
// var b = [5, -1, 7];

// var a = [1, 2, 3, -5, 0, 10, 5, 10, 15, 20, -15, -10, 3];
// var b = [5, -1, 7, 3, 5, -10, 20, -20, 2];