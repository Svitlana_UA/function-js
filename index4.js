function doubleFactorial(n) {
    if (isNaN(n)) {
        throw 'Parameter n is not a natural number!';
    } else if (n < 0) {
        throw 'Parameter n must be bigger than 0!';
    }
    if (n == 0)
        return 1;
    var result = n % 2 == 0 ? 2 : 1;
    for (let i = result + 2; i <= n; i += 2) {
        result *= i;
    }
    return result;
}

var n = 0;
console.log(doubleFactorial(n) );

//	doubleFactorial(0) ➞ 1
//	doubleFactorial(2) ➞ 2
//	doubleFactorial(9) ➞ 945 // 97531 = 945
//	doubleFactorial(14) ➞ 645120
